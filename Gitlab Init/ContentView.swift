//
//  ContentView.swift
//  Gitlab Init
//
//  Created by Addin Satria on 22/02/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, Gitlab!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
