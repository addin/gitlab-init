//
//  Gitlab_InitApp.swift
//  Gitlab Init
//
//  Created by Addin Satria on 22/02/22.
//

import SwiftUI

@main
struct Gitlab_InitApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
